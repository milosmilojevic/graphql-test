<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('users')->insert([
                [
                    'name' => 'Miloš Milojević',
                    'email' => 'milosmilojevic@outlook.com',
                    'password' => bcrypt('123'),
                ]
            ]
        );*/
        factory(App\User::class, 10)
            ->create();
    }
}
