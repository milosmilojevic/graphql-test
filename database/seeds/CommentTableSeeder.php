<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(\App\Post::all() as $p) {
            factory(App\Comment::class, 10)
                ->create(['post_id' => $p->id])
                ->each(function ($c) {
                    factory(App\Comment::class, 5)->create(['comment_id' => $c->id]);
                });
        }
    }
}
