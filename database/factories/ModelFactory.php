<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    return [
        //'sender_name' => $faker->name,
        'description' => $faker->text(200),
        'likes' => $faker->numberBetween(0,500),
        'post_id' => 1,
        'user_id' => $faker->numberBetween(1,10),
        'comment_id' => null
    ];
});
$factory->define(App\Post::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->text(200),
        'likes' => $faker->numberBetween(0,500),
        'dislikes' => $faker->numberBetween(0,500),
        'user_id' => $faker->numberBetween(1,10),
    ];
});
