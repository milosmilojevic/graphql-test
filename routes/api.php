<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('posts', 'Api\PostController@index');
Route::post('post', 'Api\PostController@store');
Route::post('post/{post}/like', 'Api\PostController@like');
Route::post('post/{post}/unlike', 'Api\PostController@unlike');

Route::get('post/{post}/comments', 'Api\PostController@comments');
Route::post('post/{post}/comment', 'Api\PostController@addComment');
Route::post('comment/{comment}/like', 'Api\PostController@likeComment');
Route::post('comment/{comment}/unlike', 'Api\PostController@unlikeComment');*/
