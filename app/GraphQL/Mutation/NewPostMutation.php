<?php

namespace App\GraphQL\Mutation;

use GraphQL;
use App\Post;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Illuminate\Support\Facades\Auth;

class NewPostMutation extends Mutation
{
    protected $attributes = [
        'name' => 'newPost'
    ];

    public function type()
    {
        return GraphQL::type('Post');
    }

    public function args()
    {
        return [
            'description' => [
                'name' => 'description',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $post               = new Post;
        $post->user_id      = Auth::loginUsingId(random_int(1, 10))->id;
        $post->description  = $args['description'];
        $post->likes        = 0;
        $post->dislikes     = 0;
        $post->save();

        return $post;
    }
}
