<?php

namespace App\GraphQL\Type;

use App\Post;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class PostType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Post',
        'description' => 'A post'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the post'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The text of post'
            ],
            'likes' => [
                'type' => Type::int(),
                'description' => 'The likes of user'
            ],
            'dislikes' => [
                'type' => Type::int(),
                'description' => 'The dislikes of user'
            ],
            'comments' => [
                'type' => Type::listOf(GraphQL::type('Comment')),
                'description' => 'The comments of a post'
            ],
            'user' => [
                'type' => GraphQL::type('User'),
                'description' => 'The creator of a post'
            ],
            'commentCount' => [
                'type' => Type::int(),
                'description' => 'Number of comments on post'
            ],
        ];
    }

    protected function resolveCommentCountField($root, $args)
    {
        return $root->comments()->count();
    }
}
