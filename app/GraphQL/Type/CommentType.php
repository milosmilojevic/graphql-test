<?php

namespace App\GraphQL\Type;

use App\Comment;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class CommentType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Comment',
        'description' => 'A comment'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the comment'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The text of comment'
            ],
            'likes' => [
                'type' => Type::int(),
                'description' => 'Number of likes'
            ],
            'user' => [
                'type' => GraphQL::type('User'),
                'description' => 'Creator'
            ],
        ];
    }

}
