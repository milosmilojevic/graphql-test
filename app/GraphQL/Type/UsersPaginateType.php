<?php
namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class UsersPaginateType extends GraphQLType
{
    protected $attributes = [
        'name' => 'UsersPaginate'
    ];

    public function fields()
    {
        return [
            'pageInfo' => [
                'type' => GraphQL::type('PageInfo'),
                'resolve' => function ($root) {
                    return array_except($root->toArray(), ['data']);
                }
            ],
            'users' => [
                'type' => Type::listOf(GraphQL::type('User')),
                'resolve' => function ($root) {
                    return $root;
                }
            ]
        ];
    }
}
