<?php
namespace App\GraphQL\Query;

use App\User;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Illuminate\Support\Facades\DB;

class UsersPaginateQuery extends Query
{
    protected $attributes = [
        'description' => 'Users paginated'
    ];

    public function type()
    {
        return GraphQL::type('UsersPaginate');
    }

    public function args()
    {
        return [
            'page' => [
                'name' => 'page',
                'description' => 'The page',
                'type' => Type::nonNull(Type::int())
            ],
            'count' => [
                'name' => 'count',
                'description' => 'The count',
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $count = array_get($args, 'count', 10);
        $page = array_get($args, 'page', 1);
        return User::query()->paginate($count, ['*'], 'page', $page);
        //return DB::table('users')->paginate($count, ['*'], 'page', $page);
    }
}
