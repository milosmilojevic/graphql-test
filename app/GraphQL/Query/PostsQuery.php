<?php

namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Post;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;

class PostsQuery extends Query
{
    protected $attributes = [
        'name' => 'posts'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Post'));
    }

    /*public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'email' => ['name' => 'email', 'type' => Type::string()]
        ];
    }*/

    public function resolve($root, $args)
    {
        Log::info('All posts');
        return Post::all();
    }
}
