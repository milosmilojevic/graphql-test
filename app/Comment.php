<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $hidden = ['deleted_at'];

    public function subcomments() {
        return $this->hasMany('App\Comment');
    }

    public function parent() {
        return $this->belongsTo('App\Comment');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
