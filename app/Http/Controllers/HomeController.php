<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function hello() {
        $posts = Post::all();
        return view('hello', ['posts' => $posts]);
    }
}
